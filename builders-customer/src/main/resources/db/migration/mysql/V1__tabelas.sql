CREATE TABLE customer (
	id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    cpf VARCHAR(11),
    birth DATE NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO customer (name, cpf, birth) value ('Carlos Alberto', '30515486-00', '1985-03-20');
INSERT INTO customer (name, cpf, birth) value ('José Antônio', '08455943033', '1950-01-01');
INSERT INTO customer (name, cpf, birth) value ('Maria da Penha', '57096232028', '1930-10-25');
