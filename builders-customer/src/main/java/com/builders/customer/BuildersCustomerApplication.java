package com.builders.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildersCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildersCustomerApplication.class, args);
	}

}
