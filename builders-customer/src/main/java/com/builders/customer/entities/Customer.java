package com.builders.customer.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.builders.customer.dtos.CustomerDTO;
import com.builders.customer.utils.DateUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 4527502465504483752L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", nullable = false)
    private String name;
	
	@Column(name = "cpf")
    private String cpf;
	
	@Column(name = "birth", nullable = false)
    private Date birth;
	
	/**
	 * Converte para DTO com os dados de um cliente.
	 * 
	 * @param customer
	 * @return CustomerDTO
	 */
	public CustomerDTO toDTO() {
		return CustomerDTO.builder()
			.id(this.getId())
			.name(this.getName())
			.cpf(this.getCpf())
			.birth(this.getBirth())
			.age(DateUtils.getAge(this.getBirth()))
			.build();
	}
}
