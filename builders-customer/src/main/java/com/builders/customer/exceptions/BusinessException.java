package com.builders.customer.exceptions;

import com.builders.customer.enums.ApiError;

import lombok.Getter;

@Getter
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 3849163313176771206L;
	
	private String message;
	
	private ApiError apiError;
	
	public BusinessException(String message) {
		super();
		this.message = message;
	}
	
	public BusinessException(String message, ApiError apiError) {
		super();
		this.message = message;
		this.apiError = apiError;
	}
	
	public BusinessException(ApiError apiError) {
		super();
		this.apiError = apiError;
	}
}
