package com.builders.customer.exceptions;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	private final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, 
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
     List<ObjectError> errors = getErrors(ex);
        ErrorResponse errorResponse = getErrorResponse(ex, status, errors);
        return new ResponseEntity<>(errorResponse, status);
    }
	
	private List<ObjectError> getErrors(MethodArgumentNotValidException ex) {
	    return ex.getBindingResult().getFieldErrors().stream()
	            .map(error -> new ObjectError(error.getDefaultMessage(), error.getField()))
	            .collect(Collectors.toList());
	}
	
	private ErrorResponse getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, 
			List<ObjectError> errors) {
		
        return ErrorResponse.builder()
        		.message("Requisição possui campos inválidos")
        		.code(status.value())
        		.status(status.getReasonPhrase())
        		.objectName(ex.getBindingResult().getObjectName())
        		.errors(errors)
        		.build();
    }

	@ExceptionHandler(ApplicationException.class)
	protected ResponseEntity<Object> handle(ApplicationException ex, WebRequest request) {
		logger.error(ex.getMessage(), ex);
		ErrorResponse response = ErrorResponse.builder().message(ex.getMessage()).build();
		return handleExceptionInternal(ex, response, new HttpHeaders(), INTERNAL_SERVER_ERROR, request);
	}
	
	@ExceptionHandler(BusinessException.class)
	protected ResponseEntity<Object> handle(BusinessException ex, WebRequest request) {
		logger.error(ex.getMessage(), ex);
		ErrorResponse response = ErrorResponse.builder().message(ex.getMessage()).build();
		return handleExceptionInternal(ex, response, new HttpHeaders(), ex.getApiError().getHttpStatus(), request);
	}
	
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handle(Exception ex, WebRequest request) {
		String message = "Ocorreu um ao ao processar requisição!";
		logger.error(message, ex);
		ErrorResponse response = ErrorResponse.builder().message(ex.getMessage()).build();
		return handleExceptionInternal(ex, response, new HttpHeaders(), INTERNAL_SERVER_ERROR, request);
	}


}
