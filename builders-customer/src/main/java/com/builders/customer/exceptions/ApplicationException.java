package com.builders.customer.exceptions;

import lombok.Getter;

@Getter
public class ApplicationException extends RuntimeException {
	
	private static final long serialVersionUID = 344031786040575681L;

	private String message;
	
	public ApplicationException(String message) {
		super();
		this.message = message;
	}
	
	public ApplicationException(String message, Exception e) {
		super(e);
		this.message = message;
	}
}