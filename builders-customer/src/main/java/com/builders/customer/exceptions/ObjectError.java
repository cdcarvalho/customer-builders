package com.builders.customer.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class ObjectError {

    private final String message;
    private final String field;
}
