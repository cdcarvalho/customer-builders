package com.builders.customer.services.impl;

import static com.builders.customer.enums.ApiError.CPF_ALREADY_EXISTS;
import static com.builders.customer.enums.ApiError.CUSTOMER_NOT_FOUND;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.builders.customer.entities.Customer;
import com.builders.customer.exceptions.BusinessException;
import com.builders.customer.repositories.CustomerRepository;
import com.builders.customer.services.CustomerService;

import lombok.Getter;

@Getter
@Service
public class CustomerServiceImpl implements CustomerService {
	
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public Page<Customer> findAll(PageRequest pageRequest) {
		log.info("Recuperando os Clientes... ");
		Page<Customer> customers = this.getCustomerRepository().findAll(pageRequest);
		
		if (CollectionUtils.isEmpty(customers.toList())) {
			throw new BusinessException(CUSTOMER_NOT_FOUND);
		}
		
		return customers;
	}

	@Override
	public Page<Customer> findByNameOrCpf(String name, String cpf, PageRequest pageRequest) {
		log.info("Recuperando os Clientes... ");
		Page<Customer> customers = this.getCustomerRepository().findByNameOrCpf(name, cpf, pageRequest);
		
		if (CollectionUtils.isEmpty(customers.toList())) {
			String message = String.format("Cliente não encontrado com nome ou cpf: {%s - %s}", name, cpf);
			throw new BusinessException(message, CUSTOMER_NOT_FOUND);
		}
		
		return customers;
	}
	
	private boolean exist(String cpf, Long id) {
		log.info("Validando CPF já cadastrado.");
		Customer customer = this.getCustomerRepository().findByCpf(cpf);
		return Optional.ofNullable(customer).isPresent() && !customer.getId().equals(id);
	}

	@Override
	public Customer save(Customer customer) {
		log.info("Salvando o Cliente... ");
		
		if(this.exist(customer.getCpf(), 0L)) {
			throw new BusinessException("CPF já Cadastrado.", CPF_ALREADY_EXISTS);
		}
		
		return this.getCustomerRepository().save(customer);
	}
	
	@Override
	public void delete(Long id) {
		log.info("Removendo o Cliente... ");
		
		if(!this.exist(id)) {
			throw new BusinessException("Não encontramos o cliente para o código informado.", CUSTOMER_NOT_FOUND);
		}
		
		this.getCustomerRepository().deleteById(id);
	}
	
	@Override
	public Customer update(Customer customer) {
		log.info("Atualizando o Cliente... ");
		
		if(this.exist(customer.getCpf(), customer.getId())) {
			throw new BusinessException("CPF já Cadastrado.", CPF_ALREADY_EXISTS);
		}
		
		return this.getCustomerRepository().save(customer);
	}
	
	private boolean exist(Long id) {
		log.info("Validando Cliente já cadastrado.");
		Optional<Customer> customer = this.getCustomerRepository().findById(id);
		return customer.isPresent();
	}


}
