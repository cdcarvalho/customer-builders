package com.builders.customer.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.builders.customer.entities.Customer;

public interface CustomerService {
	
	
	/**
	 * Obter todos os clientes
	 * 
	 * @return Page<Customer>
	 */
	Page<Customer> findAll(PageRequest pageRequest);
	
	/**
	 * Obter cliente por nome ou cpf
	 * 
	 * @param name
	 * @param cpf
	 * @return Page<Customer>
	 */
	Page<Customer> findByNameOrCpf(String name, String cpf,  PageRequest pageRequest);
	
	/**
	 * Salvar Cliente
	 * 
	 * @param customer
	 * @return Customer
	 */
	Customer save(Customer customer);

	/**
	 * Remover Cliente
	 * 
	 * @param customer
	 * @return Customer
	 */
	void delete(Long id);

	/**
	 * Atualizando Cadastro do Cliente
	 * 
	 * @param customer
	 * @return Customer
	 */
	Customer update(Customer customer);	

}
