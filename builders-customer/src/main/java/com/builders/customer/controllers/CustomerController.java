package com.builders.customer.controllers;

import com.builders.customer.dtos.CustomerDTO;
import com.builders.customer.entities.Customer;
import com.builders.customer.response.Response;
import com.builders.customer.services.CustomerService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@Getter
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/builders/api", produces = APPLICATION_JSON_VALUE)
public class CustomerController {

	@Value("${paginacao.qtd_por_pagina}")
	private int qtdPorPagina;
	
	@Autowired
	private CustomerService customerService;
	
	@ResponseStatus(OK)
	@GetMapping("/customers-all")
	public Response<Object> findAll(@RequestParam(value = "page", defaultValue = "0") int page) {
		
		PageRequest pageable = PageRequest.of(page, this.qtdPorPagina);
		log.info("Buscando todos os clientes cadastrados.");
		
		Page<Object> customers = this.getCustomerService()
				.findAll(pageable)
				.map(Customer::toDTO);
		
		return Response.builder().results(customers).build();
	}
	
	@ResponseStatus(OK)
	@GetMapping("/customers")
	public Response<Object> findByNameOrCpf(
			@RequestParam(value =  "name",required = false) String name, 
			@RequestParam(value = "cpf", required = false) String cpf,
			@RequestParam(value = "page", defaultValue = "0") int page) {
		
		PageRequest pageable = PageRequest.of(page, this.qtdPorPagina);
		log.info(String.format("Buscando clientes por nome ou cpf: %s - %s", name, cpf));
		
		Page<Object> customers = this.getCustomerService()
				.findByNameOrCpf(name, cpf, pageable)
				.map(Customer::toDTO);
		
		return Response.builder().results(customers).build();
	}
	
	@PostMapping("/create")
	@ResponseStatus(CREATED)
	public Response<Object> create(@Valid @RequestBody CustomerDTO customerDTO) {
		Customer customer = this.getCustomerService().save(customerDTO.toCustomer());
		return Response.builder()
					   .data(customer.toDTO())
					   .message("Cliente criado com sucesso.")
					   .build();
	}
	
	@ResponseStatus(OK)
	@DeleteMapping("/delete/{id}")
	public Response<Object> delete(@PathVariable Long id) {
		this.getCustomerService().delete(id);
		return Response.builder()
				   .message("Cliente removido!")
				   .build();
	}
	
	@ResponseStatus(OK)
	@PutMapping("/update")
	public Response<Object> update(@Valid @RequestBody CustomerDTO customerDTO) {
		return updateCustomer(customerDTO);
	}

	@ResponseStatus(OK)
	@PatchMapping("/patch")
	public Response<Object> patch(@RequestBody CustomerDTO customerDTO) {
		return updateCustomer(customerDTO);
	}

	private Response<Object> updateCustomer(CustomerDTO customerDTO) {
		Customer customer = this.getCustomerService().update(customerDTO.toCustomer());
		return Response.builder()
				.data(customer.toDTO())
				.message("Cliente atualizado.")
				.build();
	}
}
