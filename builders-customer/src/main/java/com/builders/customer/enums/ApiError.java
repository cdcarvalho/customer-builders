package com.builders.customer.enums;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public enum ApiError {

	CPF_ALREADY_EXISTS(CONFLICT),
	CUSTOMER_NOT_FOUND(NOT_FOUND);
	
	private HttpStatus httpStatus;
	
	private ApiError(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

}
