package com.builders.customer.utils;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

public class DateUtils {

	public static LocalDate toLocalDate(Date dateToConvert) {
	    return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
	}
	
	public static int getAge(Date birth) {
    	return Period.between(toLocalDate(birth),toLocalDate(new Date())).getYears();
    }
}
