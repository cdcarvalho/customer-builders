package com.builders.customer.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.builders.customer.entities.Customer;

@Transactional(readOnly = true)
public interface CustomerRepository extends CrudRepository<Customer, Long>  {
		
	Page<Customer> findAll(Pageable pageable);
	Page<Customer> findByNameOrCpf(String name, String cpf, Pageable pageable);
	Customer findByCpf(String cpf);

}
