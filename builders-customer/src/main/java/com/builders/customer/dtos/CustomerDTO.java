package com.builders.customer.dtos;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.br.CPF;

import com.builders.customer.entities.Customer;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@ToString
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

	private Long id;
	
	@NotEmpty(message = "{name.not.blank}")
	@Length(min = 1, max = 100, message = "{name.not.valid}")
    private String name;
    
    @CPF(message = "{cpf.not.valid}")
    @NotEmpty(message = "{cpf.not.blank}")
    private String cpf;
    
    @NotNull(message = "{birth.not.blank}")
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date birth;
    
    private int age;
    
    /**
	 * Converte para Customer com os dados de um clienteDTO.
	 * 
	 * @param CustomerDTO
	 * @return Customer
	 */
	public Customer toCustomer() {
		return Customer.builder()
			.id(this.getId())
			.name(this.getName())
			.cpf(this.getCpf())
			.birth(this.getBirth())
			.build();
	}
}
