package com.builders.customer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

import com.builders.customer.entities.Customer;
import com.builders.customer.services.CustomerService;

import lombok.Getter;

@Getter
@SpringBootTest
@ActiveProfiles("test")
public class CustomerServiceTest {
	
	@Autowired
	private CustomerService customerService;
	
	private static final String CPF = "80079289010";
	private static final String NAME = "Antônio Carlos";
	
	@BeforeEach
	public void setUp() throws Exception {
		Customer customer = Customer.builder()
						.name(NAME)
						.cpf(CPF)
						.birth(new Date())
						.build();
		
		this.getCustomerService().save(customer);
	}
	 
	
	@Test
	public void testFindByNameOrCpf() {
		Customer customer = findCustomer();
		assertEquals(NAME, customer.getName());
		assertEquals(CPF, customer.getCpf());
	}
	
	@AfterEach
	public final void tearDown() { 
		Customer customer = findCustomer();
		this.getCustomerService().delete(customer.getId()); 
	}


	private Customer findCustomer() {
		PageRequest pageable = PageRequest.of(0, 10);
		Page<Customer> customers = this.getCustomerService().findByNameOrCpf(NAME, CPF, pageable);
		return customers
				.stream()
				.filter(customer -> CPF.equals(customer.getCpf()))
			    .findFirst()
			    .get();
	}

}
