# README #

CRUD Customers

### Build Project ###
* docker-compose up -d

### Skills ###

* Java 8
* Spring Boot
* Spring Data
* Flyway
* Mysql
* Docker
* Junit


### Swagger ###

* http://localhost:8080/swagger-ui.html#/

